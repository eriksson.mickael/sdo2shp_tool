// 
// Decompiled by Procyon v0.5.36
// 

package dbConnect;


import esriShapeFile.Sdo2Shape;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Date;

import java.sql.Statement;
import java.util.Objects;

class RetrieveData
{
    private final String tableName;
    private String filePath;
    private final String whereClause;
    private String host;
    private String port;
    private String dbName;

    private void setHost(final String host) {
        this.host = host;
    }

    private void setPort(final String port) {
        this.port = port;
    }

    private void setDbName(final String dbName) {
        this.dbName = dbName;
    }

    public RetrieveData(final String tableName, @org.jetbrains.annotations.NotNull String filePath, final String host, final String port, final String dbName, final String whereClause) {

        this.host = host;
        this.port = port;
        this.whereClause = whereClause;
        this.dbName = dbName;
        this.tableName = tableName;

        if (filePath.compareToIgnoreCase("") != 0) {
            if (!filePath.endsWith("\\")) {
                filePath = filePath + "\\";
            }
            this.filePath = filePath;
        }
        this.setLogging();
    }
    
    private RetrieveData(final String tableName, final String filePath) {
        this(tableName, filePath, "gst-orarac.prod.sitad.dk", "1521", "dsdb.prod.sitad.dk","");
    }
    
    @SuppressWarnings("ConstantConditions")
    public void start(final String userName, final String password) throws Exception {
        final ResultSet result = this.getConnection(userName, password);
        Sdo2Shape shp = new Sdo2Shape(this.filePath);
        shp.CreateFile(result,null,null,true);

    }
    
    private ResultSet getConnection(final String userName, final String password) throws Exception {
        DebugLog.getInstance().writeLog(new Date() + " Connecting to database");
        final String url = "jdbc:oracle:thin:@" + this.host + ":" + this.port + "/" + this.dbName;
        final String query = "select  *  FROM " + this.tableName + " " + this.whereClause;
        DebugLog.getInstance().writeLog(new Date() + " Reading data from table: " + this.tableName);
        Class.forName("oracle.jdbc.driver.OracleDriver").getDeclaredConstructor().newInstance();
        if (userName.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
            throw new Exception("DB Userame or password Incorrect");
        }
        final Connection con = DriverManager.getConnection(url, userName, password);
        Statement stmt = con.createStatement();
        return stmt.executeQuery(query);
    }

    private void setLogging() {
        System.out.println(new Date() + " Logging set");
        DebugLog.getInstance().initialize(this.filePath);
        DebugLog.getInstance().setIsLoggingOn(true);
        DebugLog.getInstance().writeLog("Starting sdo2shp tool log.");
    }
    
    private static String help() {
        String help = "";
        help = help + "-help\t\t\tPrints the different command line arguements\n";
        help = help + "Username/Password\tThe username and password should be provided in the format: <Username>/<Password>. It should be written before any other arguements.\n";
        help = help + "-t\t\t\tOracle spatial table name that contains the SDO_Geometry column\n";
        help = help + "-f\t\t\tFile path where the ESRI shapefiles (.shp, .shx and .dbf files) created by sdo2shp tool are paced\n";
        help = help + "-h(optional)\t\tDatabase host name\n";
        help = help + "-p(optional)\t\tDatabase port\n";
        help = help + "-db(optional)\t\tDatabase name\n";
        return help;
    }
    
    public static void main(final String[] inputs) {
        String userName;
        String password;
        String tableName = "";
        String path = "";
        String host = "";
        String port = "";
        String dbName = "";
        if (inputs[0].compareToIgnoreCase("-help") == 0) {
            System.out.println(help());
        }
        else if (!inputs[0].substring(0, 1).equals("-")) {
            userName = inputs[0].substring(0, inputs[0].indexOf(47));
            password = inputs[0].substring(inputs[0].indexOf(47) + 1);
            for (int i = 1; i < inputs.length; i += 2) {
                if (inputs[i].compareToIgnoreCase("-t") == 0) {
                    tableName = inputs[i + 1];
                }
                if (inputs[i].compareToIgnoreCase("-f") == 0) {
                    path = inputs[i + 1];
                }
                if (inputs[i].compareToIgnoreCase("-h") == 0) {
                    host = inputs[i + 1];
                }
                if (inputs[i].compareToIgnoreCase("-p") == 0) {
                    port = inputs[i + 1];
                }
                if (inputs[i].compareToIgnoreCase("-db") == 0) {
                    dbName = inputs[i + 1];
                }
            }
            RetrieveData data;
            if (!Objects.equals(tableName, "") && !Objects.equals(path, "")) {
                data = new RetrieveData(tableName, path);
                if (!Objects.equals(host, "")) {
                    data.setHost(host);
                }
                if (!Objects.equals(port, "")) {
                    data.setPort(port);
                }
                if (!Objects.equals(dbName, "")) {
                    data.setDbName(dbName);
                }
                try {
                    data.start(userName, password);
                    DebugLog.getInstance().writeLog("ESRI shapefiles have been successfuly createdat:" + path);
                }
                catch (Exception e) {
                    DebugLog.getInstance().writeLog("Unable to create ESRI shapeFiles\n");
                    e.printStackTrace();
                }
            }
            else {
                System.out.println(help());
            }
        }
        else {
            System.out.println(help());
        }
    }
}
