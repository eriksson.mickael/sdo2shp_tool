package dbConnect;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

class SDOToShpGui
{
    private final JFrame frame;
    private String error;
    private dbConnect.RetrieveData data;
    private String filePath;
    private final JTextField userValue;
    private final JPasswordField passwordValue;
    private final JTextField pathValue;
    private final JTextField tableValue;
    private final JTextField whereValue;
    private final JTextField hostValue;
    private final JTextField portValue;
    private final JTextField dbNameValue;
    private Preferences preferences = Preferences.userRoot().node(this.getClass().getName());
    String USER = "user";
    String PASSWORD = "pass";
    private JTextArea output;
    private JTextArea errorValue;
    private Font font;

    private SDOToShpGui() {
        this.error = "";
        this.filePath = System.getProperty("user.home") + "/Documents/";
        this.userValue = new JTextField(25);
        this.passwordValue = new JPasswordField(25);
        this.pathValue = new JTextField(25);
        this.tableValue = new JTextField(25);
        this.hostValue = new JTextField(25);
        this.whereValue = new JTextField(25);
        this.portValue = new JTextField(25);
        this.dbNameValue = new JTextField(25);
        this.frame = new JFrame();
    }
    
    private void start() {
        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        final JPanel content = new JPanel();
        pathValue.setText( this.filePath);
        userValue.setText(preferences.get(USER,""));
        passwordValue.setText(preferences.get(PASSWORD,""));
        content.setLayout(new BorderLayout());
        content.add(this.addCredentials(), "West");
        content.add(this.addButtons(), "East");
        content.add(this.addOutput(), "South");
        this.frame.setSize(1000, 500);
        this.frame.setVisible(true);
        this.frame.setContentPane(content);
        this.frame.setTitle("sdo2shp tool");
        this.font = this.output.getFont();
    }
    
    private JPanel addButtons() {
        final GridLayout grid = new GridLayout(0, 1);
        final JButton sdoToShp = new JButton("Convert SDO_Geometry to ESRI shapefile");
        final JButton showFolder = new JButton("Open Shapefile location");
        final JButton showLogs = new JButton("Show Logs");
        final JButton help = new JButton("Help");
        sdoToShp.addActionListener(new SdoToShpListener());
        showFolder.addActionListener(new ShowFolderListener());
        showLogs.addActionListener(new showLogsListener());
        help.addActionListener(new helpListener());
        final JPanel panel = new JPanel();
        panel.setLayout(grid);
        panel.add(sdoToShp);
        panel.add(showFolder);
        panel.add(showLogs);
        panel.add(help);
        return panel;
    }
    
    private JPanel createLabelAndField(final String labelString, final JTextField field) {
        final JPanel subPanel = new JPanel();
        final JLabel label = new JLabel(labelString, SwingConstants.LEFT);
        label.setPreferredSize(new Dimension(150, 20));
        subPanel.add(label);
        subPanel.add(field);
        return subPanel;
    }
    
    private Box addCredentials() {
        final Box inputBox = new Box(1);
        this.hostValue.setText("gst-orarac.prod.sitad.dk");
        this.portValue.setText("1521");
        this.dbNameValue.setText("dsdb.prod.sitad.dk");
        inputBox.add(this.createLabelAndField("Database Username:", this.userValue));
        inputBox.add(this.createLabelAndField("Database Password:", this.passwordValue));
        inputBox.add(this.createLabelAndField("File path:", this.pathValue));
        inputBox.add(this.createLabelAndField("Table Name:", this.tableValue));
        inputBox.add(this.createLabelAndField("Where clause:", this.whereValue));
        inputBox.add(this.createLabelAndField("Database Host:", this.hostValue));
        inputBox.add(this.createLabelAndField("Database Port:", this.portValue));
        inputBox.add(this.createLabelAndField("Database Name:", this.dbNameValue));
        return inputBox;
    }
    
    private Box addOutput() {
        final Box outputBox = new Box(1);
        final JPanel panelOutput = new JPanel();
        final JPanel panelError = new JPanel();
        final JLabel outputLabel = new JLabel("Output:");
        this.output = new JTextArea(5, 20);
        final JScrollPane scrollPane = new JScrollPane(this.output);
        scrollPane.setVerticalScrollBarPolicy(22);
        scrollPane.setPreferredSize(new Dimension(800, 100));
        final JLabel errorLabel = new JLabel("Error:   ");
        this.errorValue = new JTextArea(5, 20);
        final JScrollPane errorScrollPane = new JScrollPane(this.errorValue);
        errorScrollPane.setVerticalScrollBarPolicy(22);
        errorScrollPane.setPreferredSize(new Dimension(800, 100));
        panelOutput.add(outputLabel);
        panelOutput.add(scrollPane);
        panelError.add(errorLabel);
        panelError.add(errorScrollPane);
        outputBox.add(panelOutput);
        outputBox.add(panelError);
        return outputBox;
    }
    
    public static void main(final String[] loginDetails) {
        final SDOToShpGui gui = new SDOToShpGui();
        gui.start();
    }
    
    private static /* synthetic */ void access$2(final SDOToShpGui sdoToShpGui, final String error) {
        sdoToShpGui.error = error;
    }
    
    private static /* synthetic */ void access$4(final SDOToShpGui sdoToShpGui, final String filePath) {
        sdoToShpGui.filePath = filePath;
    }
    
    private static /* synthetic */ void access$10(final SDOToShpGui sdoToShpGui, final dbConnect.RetrieveData data) {
        sdoToShpGui.data = data;
    }
    
    class SdoToShpListener implements ActionListener
    {
        @Override
        public void actionPerformed(final ActionEvent event) {
            SDOToShpGui.this.output.setText("Processing...");
            SDOToShpGui.this.output.setFont(SDOToShpGui.this.font);
            SDOToShpGui.access$2(SDOToShpGui.this, "");
            if (SDOToShpGui.this.pathValue.getText().compareToIgnoreCase("") != 0) {
                SDOToShpGui.access$4(SDOToShpGui.this, SDOToShpGui.this.pathValue.getText());
            }
            else {
                SDOToShpGui.this.pathValue.setText(SDOToShpGui.this.filePath);
            }
            SDOToShpGui.access$10(SDOToShpGui.this, new dbConnect.RetrieveData(SDOToShpGui.this.tableValue.getText().toUpperCase(), SDOToShpGui.this.filePath, SDOToShpGui.this.hostValue.getText(), SDOToShpGui.this.portValue.getText(),dbNameValue.getText(),SDOToShpGui.this.whereValue.getText()));
            try {
                SDOToShpGui.this.data.start(SDOToShpGui.this.userValue.getText(), new String(SDOToShpGui.this.passwordValue.getPassword()));
            }
            catch (Exception e) {
                SDOToShpGui.access$2(SDOToShpGui.this, "Unable to create ESRI shapeFiles\n" + e.getMessage());
            }
            if (SDOToShpGui.this.error.equalsIgnoreCase("")) {
                SDOToShpGui.this.output.setText("Created ESRI shape files successfully");
                SDOToShpGui.this.errorValue.setText(SDOToShpGui.this.error);
            }
            else {
                SDOToShpGui.this.output.setText("");
                SDOToShpGui.this.errorValue.setText(SDOToShpGui.this.error);
            }
        }
    }
    class ShowFolderListener implements ActionListener
    {
        @Override
        public void actionPerformed(final ActionEvent event) {
            SDOToShpGui.access$2(SDOToShpGui.this, "");
            if (!SDOToShpGui.this.filePath.endsWith("\\")) {
                final SDOToShpGui this$0 = SDOToShpGui.this;
                SDOToShpGui.access$4(this$0, this$0.filePath + "\\");
            }
            final File dir = new File(SDOToShpGui.this.filePath.substring(0, SDOToShpGui.this.filePath.lastIndexOf(File.separator)));
            if (Desktop.isDesktopSupported()) {
                try {
                    Desktop.getDesktop().open(dir);
                }
                catch (IOException e) {
                    SDOToShpGui.access$2(SDOToShpGui.this, "Unable to open directory " + dir + '\n' + e.getMessage());
                }
                SDOToShpGui.this.output.setText("");
                SDOToShpGui.this.errorValue.setText(SDOToShpGui.this.error);
            }
        }
    }
    
    class showLogsListener implements ActionListener
    {
        @Override
        public void actionPerformed(final ActionEvent event) {
            SDOToShpGui.this.errorValue.setText("");
            if (!SDOToShpGui.this.filePath.endsWith("\\")) {
                final SDOToShpGui this$0 = SDOToShpGui.this;
                SDOToShpGui.access$4(this$0, this$0.filePath + "\\");
            }
            try {
                Runtime.getRuntime().exec("notepad " + SDOToShpGui.this.filePath + "sdo2shp.log");
            }
            catch (IOException e) {
                SDOToShpGui.this.errorValue.setText(e.getMessage());
            }
        }
    }
    
    class helpListener implements ActionListener
    {
        @Override
        public void actionPerformed(final ActionEvent event) {
            SDOToShpGui.this.errorValue.setText("");
            if (!SDOToShpGui.this.filePath.endsWith("\\")) {
                final SDOToShpGui this$0 = SDOToShpGui.this;
                SDOToShpGui.access$4(this$0, this$0.filePath + "\\");
            }
            try {
                Runtime.getRuntime().exec("notepad UserManual.txt");
            }
            catch (IOException e) {
                SDOToShpGui.this.errorValue.setText(e.getMessage());
            }
        }
    }
}
