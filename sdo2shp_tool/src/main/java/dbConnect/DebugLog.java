// 
// Decompiled by Procyon v0.5.36
// 

package dbConnect;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileOutputStream;
import java.util.Objects;

@SuppressWarnings("ALL")
public class DebugLog
{
    private static DebugLog instance;
    private String filePath;
    private String fileName;
    private Boolean isLoggingOn;
    
    static {
        DebugLog.instance = null;
    }
    
    private DebugLog() {
    }
    
    @SuppressWarnings("ConstantConditions")
    public void initialize(final String filePath) {
        this.filePath = filePath;
        this.fileName = "sdo2shp.log";
        this.isLoggingOn = false;
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath + this.fileName);
        }
        catch (IOException e) {
            e.printStackTrace();
            try {
                Objects.requireNonNull(fos).close();
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
            return;
        }
        finally {
            try {
                Objects.requireNonNull(fos).close();
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        try {
            fos.close();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }
    
    public static DebugLog getInstance() {
        if (DebugLog.instance == null) {
            DebugLog.instance = new DebugLog();
        }
        return DebugLog.instance;
    }
    
    public Boolean getIsLoggingOn() {
        return this.isLoggingOn;
    }
    
    public void setIsLoggingOn(final Boolean isLoggingOn) {
        this.isLoggingOn = isLoggingOn;
    }
    
    public void writeLog(final String text) {
        if (!this.isLoggingOn) {
            return;
        }
        try {
            final FileWriter fileWriter = new FileWriter(this.filePath + this.fileName, true);
            final BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
            bufferWriter.write(text);
            bufferWriter.newLine();
            bufferWriter.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
