package esriShapeFile;

import dbConnect.DebugLog;
import oracle.spatial.geometry.JGeometry;
import oracle.spatial.util.GeometryExceptionWithContext;
import oracle.spatial.util.WKT;
import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.WKTReader2;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public  class Sdo2Shape {
    //set the charset for the dbf generated
    private static final Charset USER_CHARSET = StandardCharsets.UTF_8;
    //set directory where shapefile should be generated
    private final String WORKDIR;
    public Sdo2Shape(String WORKDIR) {
        this.WORKDIR = WORKDIR;
    }
            public void CreateFile(ResultSet rset, String geometrytype, String shpname, Boolean zip) throws SQLException, IOException, GeometryExceptionWithContext, SchemaException, org.locationtech.jts.io.ParseException {

                DebugLog.getInstance().writeLog(new Date() + " geometrytype " +geometrytype);
                DebugLog.getInstance().writeLog(new Date() + " starting creating files " );
                //check the input parameters
                String ShpFileName;
                ShpFileName = Objects.requireNonNullElseGet(shpname, () -> new SimpleDateFormat("yyyyMMddhhmm").format(new Date()));
                DebugLog.getInstance().writeLog(new Date() + "File names "  +ShpFileName);
                String geomType = "";
                int geomtype = 0; //this is to set the type for the shapefile
                int parsingGeomType = 0;//this is the type of geomtery parsed from resultset
                if(geometrytype == null)
                        geomtype=0;
                else {
                switch (geometrytype) {
                    case "Polygon":
                        geomtype = 7;
                        break;
                    case "Line":
                        geomtype = 6;
                        break;
                    case "CurvePolygon":
                        geomtype = 8;
                        break;
                        default:
                    geomtype=0;
                        break;
                }}
                ResultSetMetaData rsmd = rset.getMetaData();
                int numOfColumns = rsmd.getColumnCount();
                List<SimpleFeature> features = new ArrayList<>();
                SimpleFeatureBuilder featureBuilder = null;
                SimpleFeatureType N_TYPE = null;
                WKT wkt = new WKT();
                String typeElemsStr = "";
                int counter = 0;
                int sridCode = 0;

                while (rset.next()) {
                    counter = counter + 1;
                    DebugLog.getInstance().writeLog(new Date() + " processing row nr " + counter + " objectid " + rset.getString("objectid"));
                    SimpleFeature feature = null;
                    List<String> attrsNames = new ArrayList<>();
                    List<String> attrsTypes = new ArrayList<>();
                    List<String> attrsVals = new ArrayList<>();
                    /*
                     * We handle the first row independenly
                     * so we build the SimpleFeatureType from the first row
                     * this means column names and geometry type
                     * should be getted from first row unless you have supplied
                     * the geomtry type you want to parse
                     */
                    if (counter == 1) {
                        JGeometry j_geom = null;

                        for (int i = 1; i < numOfColumns + 1; i++) {
                            String colName = rsmd.getColumnName(i);
                            String columntype = rsmd.getColumnTypeName(i);
                            if (Objects.equals(columntype, "NUMBER")) {
                                typeElemsStr = typeElemsStr + colName + ":Double,";
                                attrsNames.add(colName);
                                attrsTypes.add("Double");
                                attrsVals.add(rset.getString(rset.getMetaData().getColumnName(i)));
                            }
                            if (Objects.equals(columntype, "VARCHAR2")) {
                                typeElemsStr = typeElemsStr + colName + ":String,";
                                attrsNames.add(colName);
                                attrsTypes.add("String");
                                attrsVals.add(rset.getString(rset.getMetaData().getColumnName(i)));
                            }
                            if (Objects.equals(columntype, "NVARCHAR2")) {
                                typeElemsStr = typeElemsStr + colName + ":String,";
                                attrsNames.add(colName);
                                attrsTypes.add("String");
                                attrsVals.add(rset.getString(rset.getMetaData().getColumnName(i)));
                            }
                            if (columntype.equals("MDSYS.SDO_GEOMETRY")) {
                                java.sql.Struct st = (java.sql.Struct) rset.getObject(i);
                                j_geom = JGeometry.loadJS(st);
                                if (geomtype == 0) {//set it if not supplied
                                    DebugLog.getInstance().writeLog(new Date() + "geomtery must be setted as it is not clarified");
                                            geomtype = j_geom.getType();
                                }
                                parsingGeomType = j_geom.getType();
                                sridCode = j_geom.getSRID();
                                //polygon and multipolygons may be in the same shape file
                                if (geomtype == 3 || geomtype == 7) {
                                    geomType = "MultiPolygon";
                                }
                                //point and MultiPoint may be in the same shape file
                                if (geomtype == 1 || geomtype == 5) {
                                    geomType = "MultiPoint";
                                }
                                //Line and MultiLine may be in the same shape file
                                if (geomtype == 2 || geomtype == 6) {
                                    geomType = "MultiLine";
                                }

                            }
                        }
                        typeElemsStr = "the_geom:" + geomType + ":srid=" + sridCode + "," + typeElemsStr;
                        DebugLog.getInstance().writeLog(new Date() +" typeElemsStr " + typeElemsStr);
                        N_TYPE = createFeatureType(typeElemsStr);
                        featureBuilder = new SimpleFeatureBuilder(N_TYPE);
                        String wktRepres = new String(wkt.fromJGeometry(j_geom));
                        org.locationtech.jts.geom.GeometryFactory geometryFactory1;
                        geometryFactory1 = JTSFactoryFinder.getGeometryFactory(null);
                        WKTReader2 reader = new WKTReader2(geometryFactory1);
                        Geometry geom = getGeometryObj(parsingGeomType, reader, wktRepres);
                        feature = featureBuilder.buildFeature("0");
                        feature.setAttribute("the_geom", geom);
                        for (int g = 0; g < attrsNames.size(); g++) {
                            if (attrsTypes.get(g).equals("Double")) {
                                if (attrsVals.get(g) == null) {
                                    feature.setAttribute(attrsNames.get(g), 0);
                                } else {
                                    feature.setAttribute(attrsNames.get(g), Double.parseDouble(attrsVals.get(g)));
                                }
                            }
                            if (attrsTypes.get(g).equals("String")) {
                                feature.setAttribute(attrsNames.get(g), attrsVals.get(g));
                            }
                        }
                        features.add(feature);
                    }
                    //go through the rest of rows
                    else {
                        Geometry geom = null;
                        for (int r = 1; r < numOfColumns + 1; r++) {
                            String colName = rsmd.getColumnName(r);
                            String columntype = rsmd.getColumnTypeName(r);
                            if (Objects.equals(columntype, "NUMBER")) {
                                attrsNames.add(colName);
                                attrsTypes.add("Double");
                                attrsVals.add(rset.getString(rset.getMetaData().getColumnName(r)));
                            }
                            if (Objects.equals(columntype, "VARCHAR2")) {
                                attrsNames.add(colName);
                                attrsTypes.add("String");
                                attrsVals.add(rset.getString(rset.getMetaData().getColumnName(r)));
                            }
                            if (Objects.equals(columntype, "NVARCHAR2")) {
                                attrsNames.add(colName);
                                attrsTypes.add("String");
                                attrsVals.add(rset.getString(rset.getMetaData().getColumnName(r)));
                            }
                            if (columntype.equals("MDSYS.SDO_GEOMETRY")) {
                                java.sql.Struct st = (java.sql.Struct) rset.getObject(r);
                                JGeometry j_geom = JGeometry.loadJS(st);
                                int geotype = j_geom.getType();
                                String wktRepres = new String(wkt.fromJGeometry(j_geom));
                                GeometryFactory geometryFactory1 = JTSFactoryFinder.getGeometryFactory(null);
                                WKTReader2 reader = new WKTReader2(geometryFactory1);
                                geom = getGeometryObj(geotype, reader, wktRepres);

                            }
                            assert featureBuilder != null;
                            feature = featureBuilder.buildFeature(counter + "");//trick it to get it as String
                            feature.setAttribute("the_geom", geom);
                        }
                        for (int s = 0; s < attrsNames.size(); s++) {
                            if (attrsTypes.get(s).equals("Double")) {
                                if(attrsVals.get(s) != null) {
                                    feature.setAttribute(attrsNames.get(s), Double.parseDouble(attrsVals.get(s)));
                                }
                            }
                            if (attrsTypes.get(s).equals("String")) {
                                feature.setAttribute(attrsNames.get(s), attrsVals.get(s));
                            }
                        }
                        features.add(feature);
                    }
                }
                DebugLog.getInstance().writeLog(new Date() + " features length is  " + features.size());
                File file = new File(WORKDIR + ShpFileName + ".shp");
                ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();
                Map<String, Serializable> params = new HashMap<>();
                params.put("url", file.toURI().toURL());
                params.put("create spatial index", Boolean.TRUE);
                ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
                //set the disired charset
                newDataStore.setCharset(USER_CHARSET);
                assert N_TYPE != null;
                newDataStore.createSchema(N_TYPE);
                /*
                 * Write the features to the shapefile
                 */
                Transaction transaction = new DefaultTransaction("create");

                String typeNameShp = newDataStore.getTypeNames()[0];
                SimpleFeatureSource featureSource = newDataStore.getFeatureSource(typeNameShp);

                if (featureSource instanceof SimpleFeatureStore) {
                    SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
                    /*
                     * SimpleFeatureStore has a method to add features from a
                     * SimpleFeatureCollection object, so we use the ListFeatureCollection
                     * class to wrap our list of features.
                     */
                    SimpleFeatureCollection collection = new ListFeatureCollection(N_TYPE, features);
                    featureStore.setTransaction(transaction);
                    try {
                        featureStore.addFeatures(collection);
                        //commit the transaction
                        transaction.commit();

                        if (zip) {
                            //now put all the generated files into one zip file
                            FileOutputStream fos = new FileOutputStream(WORKDIR + ShpFileName + ".zip");
                            ZipOutputStream zos = new ZipOutputStream(fos);
                            String file1Name = ShpFileName + ".shp";
                            String file2Name = ShpFileName + ".dbf";
                            String file3Name = ShpFileName + ".shx";
                            String file4Name = ShpFileName + ".prj";
                            addToZipFile(WORKDIR, file1Name, zos);
                            addToZipFile(WORKDIR, file2Name, zos);
                            addToZipFile(WORKDIR, file3Name, zos);
                            addToZipFile(WORKDIR, file4Name, zos);
                            zos.close();
                            fos.close();
                        }

                    } catch (Exception problem) {
                        DebugLog.getInstance().writeLog(new Date() + "Exception  " + problem.getMessage());
                        transaction.rollback();

                    } finally {
                        features.clear();
                        rset.close();
                        transaction.close();
                    }

                } else {
                    DebugLog.getInstance().writeLog(new Date() + "does not support read/write access  " );
                    System.exit(1);
                }
            }

            /**
             * @param s {String}
             * @return {SimpleFeatureType}
             */
            private static SimpleFeatureType createFeatureType(String s) throws SchemaException {
                s = s.substring(0, s.length() - 1);
                return DataUtilities.createType("Location",
                        s
                );
            }

            /**
             * @param geomtype  {int}
             * @param reader    {WKTReader}
             * @param wktRepres {String}
             * @return {Geometry}
             */
            private static Geometry getGeometryObj(int geomtype, WKTReader2 reader, String wktRepres) throws org.locationtech.jts.io.ParseException {

                DebugLog.getInstance().writeLog(new Date() + " Geometritype " + geomtype);
                Geometry geometryRet = null;

                    //Multipolygon
                    if (geomtype == 7) {

                        geometryRet = reader.read(wktRepres);
                    }
                    //Polygon
                    if (geomtype == 3) {
                        if (wktRepres.contains("CURVEPOLYGON")) {
                            geometryRet = reader.read(wktRepres);
                           // geometryRet = geometry;
                        }
                        else{
                            geometryRet = reader.read(wktRepres);
                        }

                    }
                    //Multiline
                    if (geomtype == 6) {
                        geometryRet = reader.read(wktRepres);
                    }
                    //Line
                    if (geomtype == 2) {
                        geometryRet = reader.read(wktRepres);
                    }
                    //MultiPoint
                    if (geomtype == 5) {
                        geometryRet = reader.read(wktRepres);
                    }
                    //Point
                    if (geomtype == 1) {
                        geometryRet = reader.read(wktRepres);
                    }

                return geometryRet;
            }

            /**
             * @param fileName {String}
             * @param zos      {ZipOutputStream}
             */
            private static void addToZipFile(String workdir, String fileName, ZipOutputStream zos) throws IOException {

                DebugLog.getInstance().writeLog(new Date() + "Writing '" + fileName + "' to zip file");
                        File file = new File(workdir + fileName);
                FileInputStream fis = new FileInputStream(file);
                ZipEntry zipEntry = new ZipEntry(fileName);
                zos.putNextEntry(zipEntry);

                byte[] bytes = new byte[1024];
                int length;
                while ((length = fis.read(bytes)) >= 0) {
                    zos.write(bytes, 0, length);
                }

                zos.closeEntry();
                fis.close();
            }

}
